# Local Stories
## A mobile application that visualizes local history.

This project has been permanently moved to [https://github.com/eivind88/local-stories](https://github.com/eivind88/local-stories)

![screenshot](https://bitbucket.org/eivind88/local-stories/raw/master/screenshot.png)

Made as part of a student project in the course
Mobile Applications (2013) at Østfold University College (HiOF)
by Eivind Arvesen and Kjetil Engvold.
